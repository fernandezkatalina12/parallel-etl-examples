﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReadingFile
{
    /// <summary>
    /// Compare various ways of reading in a CSV file in C#
    /// 
    /// Overhead from spinning up threads negates any performance benefits from splitting up the work among multiple threads
    /// As a result, it actually takes longer for a 10-core CPU to parse this CSV than a 4-core server
    /// 
    /// </summary>
    class Program
    {
        // reading in a large CSV file
        static void Main(string[] args)
        {
            var filePath = Environment.CurrentDirectory + "/file.csv";

            if (args.Length > 0)
            {
                var maxThreads = Int32.Parse(args[0]);
                PLINQUser(filePath, maxThreads);
            }

            LINQ(filePath);
            PLINQAll(filePath);
            PLINQAllThreads(filePath);
            PLINQ2(filePath);
            PLINQNoRegex(filePath);
            ParallelForEach(filePath);
            ParallelForEachRegex(filePath);
            VisualBasicFileIO(filePath);
        }

        // Single-threaded LINQ
        public static void LINQ(string filePath)
        {
            var sw = new Stopwatch();
            sw.Start();

            var results = System.IO.File.ReadAllLines(filePath)
                .Select(line => Regex.Split(line, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"))
                .ToList();

            sw.Stop();
            Console.WriteLine($"LINQ using 1 core: completed in {Math.Round(sw.Elapsed.TotalSeconds)} seconds");
        }

        // PLINQ using all CPU cores
        public static void PLINQAll(string filePath)
        {
            var sw = new Stopwatch();
            sw.Start();

            var results = System.IO.File.ReadAllLines(filePath)
                .AsParallel()
                .Select(line => Regex.Split(line, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"))
                .ToList();

            sw.Stop();
            Console.WriteLine($"PLINQ using all cores: completed in {Math.Round(sw.Elapsed.TotalSeconds)} seconds");
        }

        // PLINQ using all CPU threads (2x cores)
        public static void PLINQAllThreads(string filePath)
        {
            var threads = Environment.ProcessorCount * 2;
            var sw = new Stopwatch();
            sw.Start();

            var results = System.IO.File.ReadAllLines(filePath)
                .AsParallel()
                .WithDegreeOfParallelism(threads)
                .Select(line => Regex.Split(line, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"))
                .ToList();

            sw.Stop();
            Console.WriteLine($"PLINQ using all THREADS {(threads)}: completed in {Math.Round(sw.Elapsed.TotalSeconds)} seconds");
        }

        // PLINQ using 2 CPU cores
        public static void PLINQ2(string filePath)
        {
            var sw = new Stopwatch();
            sw.Start();

            var results = System.IO.File.ReadAllLines(filePath)
                .AsParallel()
                .WithDegreeOfParallelism(2)
                .Select(line => Regex.Split(line, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"))
                .ToList();

            sw.Stop();
            Console.WriteLine($"PLINQ using 2 cores: completed in {Math.Round(sw.Elapsed.TotalSeconds)} seconds");
        }

        // PLINQ using user-defined thread count
        public static void PLINQUser(string filePath, int numThreads)
        {
            var sw = new Stopwatch();
            sw.Start();

            var results = System.IO.File.ReadAllLines(filePath)
                .AsParallel()
                .WithDegreeOfParallelism(numThreads)
                .Select(line => Regex.Split(line, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"))
                .ToList();

            sw.Stop();
            Console.WriteLine($"PLINQ using {numThreads} threads: completed in {Math.Round(sw.Elapsed.TotalSeconds)} seconds");
        }

        // PLINQ using all CPU cores without Regex parsing - May yield bad data due to commas within fields
        public static void PLINQNoRegex(string filePath)
        {
            var sw = new Stopwatch();
            sw.Start();

            var results = System.IO.File.ReadAllLines(filePath)
                .AsParallel()
                .Select(x => x.Split(','))
                .ToList();

            sw.Stop();
            Console.WriteLine($"PLINQ using all cores (no Regex): completed in {Math.Round(sw.Elapsed.TotalSeconds)} seconds");
        }

        // Parallel.ForEach using all CPU cores - May yield bad data
        public static void ParallelForEach(string filePath)
        {
            var sw = new Stopwatch();
            sw.Start();

            var rows = new List<string[]>();
            Parallel.ForEach(File.ReadLines(filePath), line =>
            {
                rows.Add(line.Split(','));
            });

            sw.Stop();
            Console.WriteLine($"Parallel.ForEach using all cores (no Regex): completed in {Math.Round(sw.Elapsed.TotalSeconds)} seconds");
        }

        // Parallel.ForEach using all CPU cores with Regex parsing - takes roughly the same amount of time as PLINQ
        public static void ParallelForEachRegex(string filePath)
        {
            var sw = new Stopwatch();
            sw.Start();

            var rows = new List<string[]>();
            Parallel.ForEach(File.ReadLines(filePath), line =>
            {
                rows.Add(Regex.Split(line, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"));
            });

            sw.Stop();
            Console.WriteLine($"Parallel.ForEach w/Regex using all cores: completed in {Math.Round(sw.Elapsed.TotalSeconds)} seconds");
        }


        // Single-threaded VisualBasic.FileIO - The fastest built-in solution despite not being parallel - not everything benefits from parallelization!
        public static void VisualBasicFileIO(string filePath)
        {
            var sw = new Stopwatch();
            sw.Start();

            var rows = new List<string[]>();
            using (TextFieldParser parser = new TextFieldParser(filePath))
            {
                parser.Delimiters = new string[] { "," };
                while (true)
                {
                    string[] row = parser.ReadFields();
                    if (row == null)
                        break;

                    rows.Add(row);
                }
            }

            sw.Stop();
            Console.WriteLine($"VisualBasic TextFieldParser: completed in {Math.Round(sw.Elapsed.TotalSeconds)} seconds");
        }
    }
}
